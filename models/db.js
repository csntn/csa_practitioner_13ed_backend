const mongoose = require('mongoose');

const mongoDBURI = process.env.MONGODB_URI;
const mongooseOptions = {dbName: process.env.MONGODB_DBNAME, useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true};

mongoose.connect(mongoDBURI, mongooseOptions).then(
  () => {
    console.log('Connected to mongoDB');
  },
  err => {
    console.log('Error connecting to mongoDB');
  }
);

mongoose.connection.on('connected', function () {
  console.log('Mongoose connected');
});

mongoose.connection.on('error', function (err) {
  console.log('Mongoose connection error: ' + err);
});

mongoose.connection.on('reconnected', function () {
  console.log('Mongoose reconnected');
});

mongoose.connection.on('disconnected', function () {
  console.log('Mongoose disconnected');
});

process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    console.log('Mongoose connection closed');
    process.exit(0);
  });
});

require('./user');
require('./account');
require('./movement');

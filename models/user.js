const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const userSchema = mongoose.Schema(
  {
    firstName: {
      type :String,
      required: true,
      trim: true
    },
    lastName: {
      type :String,
      required: true,
      trim: true
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true
    },
    password: {
      type: String,
      required: true,
      minlength: 8,
      trim: true
    },
    '2fa': {
      type: Boolean,
      default: false
    },
    secret: {
      type: String,
      trim: true
    },
    invalidTokens: [{
      jti: String,
      exp: Number
    }]
  },
  {
    collection: 'users',
    timestamps: true
  }
);

userSchema.pre('save',
  function (next) {
    var user = this;
      
    if (!user.isModified('password')) {
      return next();
    };
  
    bcrypt.hash(user.password, 10).then((hashedPassword) => {
      user.password = hashedPassword;
      next();
    });
  },
  function (err) {
    next(err);
  }
)

userSchema.methods.comparePassword = function(candidatePassword, next) {
  bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
    if(err) return next(err);

    next(null,isMatch);
  })
}

const User = mongoose.model('User', userSchema);
module.exports.User = User;

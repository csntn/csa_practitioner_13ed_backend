const mongoose = require('mongoose');
const User = require('./user');
const ObjectId = mongoose.Types.ObjectId;

const accountSchema = mongoose.Schema(
  {
    iban: {
      type: String,
      unique: true,
      required: true
    },
    ownerid: {
      type: ObjectId,
      ref: 'User',
      required: true
    },
    currency: {
      type: String,
      default: 'EUR'
    },
    initialBalance: {
      type: Number,
      default: 0
    },
    balance: {
      type: Number,
      default: 0
    }
  },
  {
    collection: 'accounts',
    timestamps: true
  }
);


const Account = mongoose.model('Account', accountSchema);
module.exports.Account = Account;

const mongoose = require('mongoose');
const ObjectId = mongoose.Schema.ObjectId;

const movementSchema = mongoose.Schema(
  {
    movementType: {
      type: String,
      required: true
    },
    description: String,
    originIBAN: String,
    originID: {
      type: ObjectId,
      ref: 'Account'
    },
    originCurrency: {
      type: String,
      required: true
    },
    originAmmount: {
      type: Number,
      required: true
    },
    destinationIBAN: String,
    destinationID: {
      type: ObjectId,
      ref: 'Account'
    },
    destinationCurrency: {
      type: String,
      required: true
    },
    destinationAmmount: {
      type: Number,
      required: true
    },
    rate: Number,
    coords: {
      latitude: Number,
      longitude: Number
    }
  },
  {
    collection: 'movements',
    timestamps: true
  }
);

const Movement = mongoose.model('Movement', movementSchema);
module.exports.Movement = Movement;

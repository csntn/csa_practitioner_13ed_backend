const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');
const bcrypt = require('bcryptjs');

chai.use(chaihttp);

var should = chai.should();
var testUserId = '';

// Test backend API

// Create test user
describe('Test createUserV1', function() {
  it('Test that the API responds to user creation', function(done) {
    chai
      .request('http://localhost:3000')
      .post('/apitechu/v1/users')
      .send({email: 'test@api.techu', firstName: 'Test', lastName: 'API TechU', password:  '12345678'})
      .end(function(err, res) {
        console.log('Request finished');
        res.should.have.status(201);

        res.body.should.be.a('object');

        res.body.should.have.property('msg');
        res.body.msg.should.be.eql('User created');
        res.body.should.have.property('userid');
        res.body.userid.should.be.a('string');
        res.body.userid.should.not.be.equal('');
        testUserId = res.body.userid;
        done();
      });
  });
});

// Get test user
describe('Test getUserByIdV1', function() {
  it('Test that the API finds created user', function(done) {
    chai
      .request('http://localhost:3000')
      .get('/apitechu/v1/users/'+testUserId)
      .end(function(err, res) {
        console.log('Request finished');
        res.should.have.status(200);

        res.body.should.be.a('object');

        res.body.should.have.property('_id');
        res.body.should.have.property('email');
        res.body.should.have.property('firstName');
        res.body.should.have.property('lastName');
        res.body.should.have.property('password');
        res.body._id.should.be.eql(testUserId);
        res.body.email.should.be.eql('test@api.techu');
        res.body.firstName.should.be.eql('Test');
        res.body.lastName.should.be.eql('API TechU');
        done();
      });
  });
});

// Delete created test user
describe('Test deleteUserByIdV1', function() {
  it('Test that the API responds to user deletion', function(done) {
    chai
      .request('http://localhost:3000')
      .delete('/apitechu/v1/users/'+testUserId)
      .end(function(err, res) {
        console.log('Request finished');
        res.should.have.status(200);
        done();
      });
  });

// Try to get deleted test user
describe('Test getUserByIdV1', function() {
  it('Test that the API cannot find deleted user', function(done) {
    chai
      .request('http://localhost:3000')
      .get('/apitechu/v1/users/'+testUserId)
      .end(function(err, res) {
        console.log('Request finished');
        res.should.have.status(404);
        res.body.should.have.property('msg');
        res.body.msg.should.be.eql('User not found');
        done();
      });
  });
});
});

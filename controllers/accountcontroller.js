const mongoose = require('mongoose');
const Account = mongoose.model('Account');
const ObjectId = mongoose.Types.ObjectId;

function getAccountsV1(req, res) {
  console.log('GET /apitechu/v1/accounts');

  Account.find({}).populate('ownerid', '_id email firstName lastName').exec(function(err, accounts) {
    res.send(err ? {msg: 'Cannot get accounts', error: err} : accounts);
  });
}

function deleteAccountByIdV1(req, res) {
  console.log('DELETE /apitechu/v1/accounts/:accountid');

  Account.findByIdAndDelete(req.params.accountid, function(err, account) {
    res.send(err ? {msg: 'Cannot delete account', error: err} : {msg: 'Account deleted', accountid: account ? account.id : null});
  });
}

function createUserByIdAccountV1(req, res) {
  console.log('POST /apitechu/v1/users/:userid/accounts');

  //TODO: validate iban, currency, balance
  Account.create({
    iban: req.body.iban,
    currency: req.body.currency,
    initialBalance: req.body.initialBalance,
    balance: req.body.initialBalance,
    ownerid: new ObjectId(req.params.userid)
  }, function (err, account) {

    if (err) {
      res.status(409).send({msg: 'Duplicate account', error: err});
    } else {
    res.status(201).send({msg: 'Account created', accountid: account.id, ownerid: account.ownerid});
    }

  });
}

function getUserByIdAccountsV1(req, res) {
  console.log('GET /apitechu/v1/users/:userid/accounts');

  Account.find({ownerid: req.params.userid}, function(err, accounts) {
    res.send(err ? {msg: 'Cannot get user accounts', error: err} : accounts);
  });
}

function deleteUserByIdAccountsV1(req, res) {
  console.log('DELETE /apitechu/v1/users/:userid/accounts');

  Account.deleteMany({ownerid: req.params.userid}, function(err, accounts) {
    res.send(err ? {msg: 'Cannot delete accounts', error: err} : {msg: 'Accounts deleted'});
  });
}

function deleteUserByIdAccountByIdV1(req, res) {
  console.log('DELETE /apitechu/v1/users/:userid/accounts/:accountid');

  Account.findOneAndDelete({_id: req.params.accountid, ownerid: req.params.userid}, function(err, account) {
    res.send(err ? {msg: 'Cannot delete account', error: err} : {msg: 'Account deleted', accountid: account ? account.id : null});
  });
}

function getUserByIdAccountByIdV1(req, res) {
  console.log('GET /apitechu/v1/users/:userid/accounts/:accountid');

  Account.findOne({_id: req.params.accountid, ownerid: req.params.userid}, function(err, account) {
    res.send(err ? {msg: 'Cannot get user account', error: err} : account);
  });
}

function createUserAccountV1(req, res) {
  console.log('POST /apitechu/v1/user/accounts');
  console.log(req.body);

  if (req.decodedToken) {
    //TODO: validate iban, currency, balance
    Account.create({
      iban: req.body.iban,
      currency: req.body.currency,
      initialBalance: req.body.initialBalance,
      balance: req.body.initialBalance,
      ownerid: new ObjectId(req.decodedToken.sub)
    }, function (err, account) {
  
      if (err) {
        res.status(409).send({msg: 'Duplicate account', error: err});
      } else {
      res.status(201).send({msg: 'Account created', accountid: account.id, ownerid: account.ownerid});
      }
  
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function getUserAccountsV1(req, res) {
  console.log('GET /apitechu/v1/user/accounts');

  console.log(req.decodedToken);
  if (req.decodedToken) {
    Account.find({ownerid: req.decodedToken.sub}, function(err, accounts) {
      res.send(err ? {msg: 'Cannot get user accounts', error: err} : accounts);
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function deleteUserAccountsV1(req, res) {
  console.log('DELETE /apitechu/v1/user/accounts');

  if (req.decodedToken) {
    Account.deleteMany({ownerid: req.decodedToken.sub}, function(err, accounts) {
      res.send(err ? {msg: 'Cannot delete accounts', error: err} : {msg: 'Accounts deleted'});
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function deleteUserAccountByIdV1(req, res) {
  console.log('DELETE /apitechu/v1/user/accounts/:accountid');

  if (req.decodedToken) {
    Account.findOneAndDelete({_id: req.params.accountid, ownerid: req.decodedToken.sub}, function(err, account) {
      res.send(err ? {msg: 'Cannot delete account', error: err} : {msg: 'Account deleted', accountid: account ? account.id : null});
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function getUserAccountByIdV1(req, res) {
  console.log('GET /apitechu/v1/user/accounts/:accountid');

  if (req.decodedToken) {
    Account.findOne({_id: req.params.accountid, ownerid: req.decodedToken.sub}, function(err, account) {
      res.send(err ? {msg: 'Cannot get user account', error: err} : account);
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

module.exports.getAccountsV1 = getAccountsV1;
module.exports.deleteAccountByIdV1 = deleteAccountByIdV1;
module.exports.getUserByIdAccountsV1 = getUserByIdAccountsV1;
module.exports.deleteUserByIdAccountsV1 = deleteUserByIdAccountsV1;
module.exports.getUserByIdAccountByIdV1 = getUserByIdAccountByIdV1;
module.exports.deleteUserByIdAccountByIdV1 = deleteUserByIdAccountByIdV1;
module.exports.createUserByIdAccountV1 = createUserByIdAccountV1;

module.exports.getUserAccountsV1 = getUserAccountsV1;
module.exports.deleteUserAccountsV1 = deleteUserAccountsV1;
module.exports.getUserAccountByIdV1 = getUserAccountByIdV1;
module.exports.deleteUserAccountByIdV1 = deleteUserAccountByIdV1;
module.exports.createUserAccountV1 = createUserAccountV1;

const mongoose = require('mongoose');
const User = mongoose.model('User');
const Account = mongoose.model('Account');
const Movement = mongoose.model('Movement');
const ObjectId = mongoose.Types.ObjectId;

function getMovementsV1(req, res) {
  console.log('GET /apitechu/v1/movements');

  Movement.find({}).sort({createdAt: 'desc'}).exec(function(err, movements) {
    res.send(err ? {msg: 'Cannot get movements', error: err} : movements);
  });
}

function deleteMovementByIdV1(req, res) {
  console.log('DELETE /apitechu/v1/movements/:movementid');

  Movement.findByIdAndDelete(req.params.movementid, function(err, movement) {
    res.send(err ? {msg: 'Cannot delete movement', error: err} : {msg: 'Movement deleted', movementid: movement ? movement.id : null});
  });
}

function createMovementV1(req, res) {
  console.log('POST /apitechu/v1/movements');

  if (req.body.movementType == 'income') {
    Account.findOne({iban: req.body.destinationIBAN}, function (err, account) {
      if (err) {
        res.status(500).send({msg: 'Cannot get account', error: err});
      } else if (!account) {
        res.status(404).send({msg: 'Account not found'});
      } else if (account.currency != req.body.destinationCurrency) {
        res.status(400).send({msg: 'Wrong currency'});
      } else {
        Movement.create({
          movementType: 'income',
          description: req.body.description,
          originCurrency: req.body.originCurrency,
          originAmmount: req.body.originAmmount,
          destinationIBAN: account.iban,
          destinationID: account['_id'],
          destinationCurrency: account.currency,
          destinationAmmount: req.body.destinationAmmount,
          rate: req.body.rate,
          coords: req.body.coords ? {latitude: req.body.coords.latitude, longitude: req.body.coords.longitude} : undefined
        }, function (err, movement) {
          if (err) {
            res.status(500).send({msg: 'Movement error', error: err});
          } else if (movement) {
            // should rollback movement if error
            account.balance += movement.destinationAmmount;
            account.save();

            res.status(201).send({msg: 'Movement created', movementid: movement.id});
          } else {
            res.status(500).send({msg: 'Movement error', error: err});
          }
        });
      }
    });
  } else if (req.body.movementType == 'expense') {
    
    Account.find({iban: req.body.originIBAN}, function (err, account) {
      if (err) {
        res.status(500).send({msg: 'Cannot get account', error: err});
      } else if (!account) {
        res.status(404).send({msg: 'Account not found'});
      } else if (account.currency != req.body.originCurrency) {
        res.status(400).send({msg: 'Wrong currency'});
      } else if (account.balance < req.body.originAmmount) {
        res.status(400).send({msg: 'Not enough balance'});
      } else {
        Movement.create({
          movementType: 'expense',
          description: req.body.description,
          originIBAN: account.iban,
          originID: account['_id'],
          originCurrency: account.currency,
          originAmmount: req.body.originAmmount,
          destinationCurrency: req.body.destinationCurrency,
          destinationAmmount: req.body.destinationAmmount,
          rate: req.body.rate,
          coords: req.body.coords ? {latitude: req.body.coords.latitude, longitude: req.body.coords.longitude} : undefined
        }, function (err, movement) {
          if (err) {
            res.status(500).send({msg: 'Movement error', error: err});
          } else if (movement) {
            // should rollback movement if error
            account.balance -= movement.originAmmount;
            account.save();

            res.status(201).send({msg: 'Movement created', movementid: movement.id});
          } else {
            res.status(500).send({msg: 'Movement error', error: err});
          }
        });
      }
    });
  } else if (req.body.movementType == 'transfer') {
    var originAccount = null;
    var destinationAccount = null;

    console.log('transfer');
    console.log('oIBAN: ' + req.body.originIBAN);
    Account.findOne({iban: req.body.originIBAN}, function (err, account) {
    console.log(account);
      if (err) {
        res.status(500).send({msg: 'Cannot get account', error: err});
        return;
      } else if (account.currency != req.body.originCurrency) {
        res.status(400).send({msg: 'Wrong origin currency'});
        return;
      } else if (account.balance < req.body.originAmmount) {
        res.status(400).send({msg: 'Not enough balance'});
        return;
      } else {
        originAccount = account;

        console.log('dIBAN: ' + req.body.destinationIBAN);
        Account.findOne({iban: req.body.destinationIBAN}, function (err, account) {
        console.log(account);
          if (err) {
            res.status(500).send({msg: 'Cannot get account', error: err});
            return;
          } else if (account.currency != req.body.destinationCurrency) {
            res.status(400).send({msg: 'Wrong destination currency'});
            return;
          } else {
            destinationAccount = account;

            console.log('originAcc: ' + originAccount);
            console.log('destinationAcc: ' + destinationAccount);
            if (originAccount || destinationAccount) {
              Movement.create({
                movementType: 'transfer',
                description: req.body.description,
                originIBAN: originAccount ? originAccount.iban: req.body.originAccount,
                originID: originAccount ? originAccount['_id'] : undefined,
                originCurrency: originAccount ? originAccount.currency : req.body.originCurrency,
                originAmmount: req.body.originAmmount,
                destinationIBAN: destinationAccount ? destinationAccount.iban: req.body.destinationIBAN,
                destinationID: destinationAccount ? destinationAccount['_id'] : undefined,
                destinationCurrency: destinationAccount ? destinationAccount.currency : req.body.destinationCurrency,
                destinationAmmount: req.body.destinationAmmount,
                rate: req.body.rate,
                coords: req.body.coords ? {latitude: req.body.coords.latitude, longitude: req.body.coords.longitude} : undefined
              }, function (err, movement) {
                if (err) {
                  res.status(500).send({msg: 'Movement error', error: err});
                } else if (movement) {
                  // should rollback movement if error
                  if (originAccount) {
                    originAccount.balance -= movement.originAmmount;
                    originAccount.save();
                  }
                  if (destinationAccount) {
                    destinationAccount.balance += movement.destinationAmmount;
                    destinationAccount.save();
                  }
          
                  res.status(201).send({msg: 'Movement created', movementid: movement.id});
                } else {
                  res.status(500).send({msg: 'Movement error', error: err});
                }
              });
            } else {
              res.status(404).send({msg: 'Account not found'});
            }
          }
        });
      }
    });

  } else {
    res.status(500).send({msg: 'Wrong movement'});
  }
}

function getUserAccountByIdMovementsV1(req, res) {
  console.log('GET /apitechu/v1/user/accounts/:accountid/movements');

  if (req.decodedToken) {
    Account.findById(req.params.accountid, function (err, account) {
      if (err) {
        res.status(500).send({msg: 'Cannot get account', error: err});
        return;
      } else if (!account) {
        res.status(404).send({msg: 'Account not found'});
        return;
      } else if (account.ownerid != req.decodedToken.sub) {
        res.status(400).send({msg: 'Wrong account owner'});
        return;
      } else {
        Movement.find({$or: [{originID: req.params.accountid}, {destinationID: req.params.accountid}]}).sort({createdAt: 'desc'}).exec(function(err, movements) {
          res.send(err ? {msg: 'Cannot get user movements', error: err} : movements);
        });
      }
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function createUserAccountByIdMovementV1(req, res) {
  console.log('POST /apitechu/v1/user/accounts/:accountid/movements');
  console.log(req.body);

  if (req.decodedToken) {
    Account.findById(req.params.accountid, function (err, account) {
      if (err) {
        res.status(500).send({msg: 'Cannot get account', error: err});
        return;
      } else if (!account) {
        res.status(404).send({msg: 'Account not found'});
        return;
      } else if (account.ownerid != req.decodedToken.sub) {
        res.status(400).send({msg: 'Wrong account owner'});
        return;
      } else {

        if (req.body.movementType == 'income') {
          if (account.currency != req.body.destinationCurrency) {
            res.status(400).send({msg: 'Wrong currency'});
          } else {
            Movement.create({
              movementType: 'income',
              description: req.body.description,
              originCurrency: req.body.originCurrency,
              originAmmount: req.body.originAmmount,
              destinationIBAN: account.iban,
              destinationID: account['_id'],
              destinationCurrency: account.currency,
              destinationAmmount: req.body.destinationAmmount,
              rate: req.body.rate,
              coords: req.body.coords ? {latitude: req.body.coords.latitude, longitude: req.body.coords.longitude} : undefined
            }, function (err, movement) {
              if (err) {
                res.status(500).send({msg: 'Movement error', error: err});
              } else if (movement) {
                // should rollback movement if error
                account.balance += movement.destinationAmmount;
                account.save();
    
                res.status(201).send({msg: 'Movement created', movementid: movement.id});
              } else {
                res.status(500).send({msg: 'Movement error', error: err});
              }
            });
          }

        } else if (req.body.movementType == 'expense') {
          if (account.currency != req.body.originCurrency) {
            res.status(400).send({msg: 'Wrong currency'});
          } else if (account.balance < req.body.originAmmount) {
            res.status(400).send({msg: 'Not enough balance'});
          } else {
            Movement.create({
              movementType: 'expense',
              description: req.body.description,
              originIBAN: account.iban,
              originID: account['_id'],
              originCurrency: account.currency,
              originAmmount: req.body.originAmmount,
              destinationCurrency: req.body.destinationCurrency,
              destinationAmmount: req.body.destinationAmmount,
              rate: req.body.rate,
              coords: req.body.coords ? {latitude: req.body.coords.latitude, longitude: req.body.coords.longitude} : undefined
            }, function (err, movement) {
              if (err) {
                res.status(500).send({msg: 'Movement error', error: err});
              } else if (movement) {
                // should rollback movement if error
                account.balance -= movement.originAmmount;
                account.save();
    
                res.status(201).send({msg: 'Movement created', movementid: movement.id});
              } else {
                res.status(500).send({msg: 'Movement error', error: err});
              }
            });
          }

        } else if (req.body.movementType == 'transfer') {
          if (account.currency != req.body.originCurrency) {
            res.status(400).send({msg: 'Wrong currency'});
          } else if (account.balance < req.body.originAmmount) {
            res.status(400).send({msg: 'Not enough balance'});
          } else {
            Account.findOne({iban: req.body.destinationIBAN}, function (err, destinationAccount) {
              if (err) {
                res.status(500).send({msg: 'Cannot get account', error: err});
                return;
              } else if (destinationAccount ? destinationAccount.currency != req.body.destinationCurrency : false) {
                res.status(400).send({msg: 'Wrong destination currency'});
                return;
              } else {
                  Movement.create({
                    movementType: 'transfer',
                    description: req.body.description,
                    originIBAN: account.iban,
                    originID: account['_id'],
                    originCurrency: account.currency,
                    originAmmount: req.body.originAmmount,
                    destinationIBAN: destinationAccount ? destinationAccount.iban: req.body.destinationIBAN,
                    destinationID: destinationAccount ? destinationAccount['_id'] : undefined,
                    destinationCurrency: destinationAccount ? destinationAccount.currency : req.body.destinationCurrency,
                    destinationAmmount: req.body.destinationAmmount,
                    rate: req.body.rate,
                    coords: req.body.coords ? {latitude: req.body.coords.latitude, longitude: req.body.coords.longitude} : undefined
                  }, function (err, movement) {
                    if (err) {
                      res.status(500).send({msg: 'Movement error', error: err});
                    } else if (movement) {
                      // should rollback movement if error
                      account.balance -= movement.originAmmount;
                      account.save();
                      if (destinationAccount) {
                        destinationAccount.balance += movement.destinationAmmount;
                        destinationAccount.save();
                      }
              
                      res.status(201).send({msg: 'Movement created', movementid: movement.id});
                    } else {
                      res.status(500).send({msg: 'Movement error', error: err});
                    }
                  });
              }
            });
          }
        } else {
          res.status(500).send({msg: 'Wrong movement'});
        }
      }
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

module.exports.createMovementV1 = createMovementV1;
module.exports.getMovementsV1 = getMovementsV1;
module.exports.deleteMovementByIdV1 = deleteMovementByIdV1;

module.exports.createUserAccountByIdMovementV1 = createUserAccountByIdMovementV1;
module.exports.getUserAccountByIdMovementsV1 = getUserAccountByIdMovementsV1;

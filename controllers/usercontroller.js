const mongoose = require('mongoose');
const User = mongoose.model('User');

function getUsersV1(req, res) {
  console.log('GET /apitechu/v1/users');

  User.find({}, function(err, users) {
    res.send(err ? {msg: 'Cannot get users', error: err} : users);
  });
}

function getUserByIdV1(req, res) {
  console.log('GET /apitechu/v1/users/:userid');

  User.findById(req.params.userid, function(err, user) {
    if (err) {
      res.status(500).send({msg: 'Cannot get user', error: err})
    } else if (user) {
      res.send(user)
    } else {
      res.status(404).send({msg: 'User not found'});
    }
  });
}

function createUserV1(req, res) {
  console.log('POST /apitechu/v1/users');

  User.create({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: req.body.password,
  }, function (err, user) {

    if (err) {
      res.status(409).send({msg: 'Duplicate user', error: err});
    } else {
    res.status(201).send({msg: 'User created', userid: user.id});
    }

  });
}

function deleteUserByIdV1(req, res) {
  console.log('DELETE /apitechu/v1/users/:userid');

  User.findByIdAndDelete(req.params.userid, function(err, user) {
    res.send(err ? {msg: 'Cannot delete user', error: err} : {msg: 'User deleted', userid: user ? user.id : null});
  });
}

function getUserV1(req, res) {
  console.log('GET /apitechu/v1/user');

  if (req.decodedToken) {
    User.findById(req.decodedToken.sub, function(err, user) {
      if (err) {
        res.status(500).send({msg: 'Cannot get user', error: err})
      } else if (user) {
        var userData = {
          firstName: user.firstName,
          lastName: user.lastName,
          email: user.email,
          createdAt: user.createdAt,
          updatedAt: user.updatedAt
        }
        res.send(userData)
      } else {
        res.status(404).send({msg: 'User not found'});
      }
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function deleteUserV1(req, res) {
  console.log('DELETE /apitechu/v1/user');

  if (req.decodedToken) {
    User.findByIdAndDelete(req.decodedToken.sub, function(err, user) {
      res.send(err ? {msg: 'Cannot delete user', error: err} : {msg: 'User deleted', userid: user ? user.id : null});
    });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

module.exports.getUsersV1 = getUsersV1;

module.exports.createUserV1 = createUserV1;
module.exports.deleteUserByIdV1 = deleteUserByIdV1;
module.exports.getUserByIdV1 = getUserByIdV1;
module.exports.deleteUserV1 = deleteUserV1;
module.exports.getUserV1 = getUserV1;

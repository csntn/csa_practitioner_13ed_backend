const mongoose = require('mongoose');
const User = mongoose.model('User');

const jwt = require('jsonwebtoken');
const uuidv4 = require('uuid/v4');

function loginV1(req, res) {
  console.log('POST /apitechu/v1/login');

  let status = 200;
  let msg = {};

  User.findOne({email: req.body.email}, (err, user) => {
    console.log(err);
    console.log(user);
    if (!err && user) {
      user.comparePassword(req.body.password, (err, isMatch) => {
        if (isMatch) {
          status = 200;

          const payload = {
            given_name: user.firstName,
            family_name: user.lastName,
            email: user.email
          };
          const options = {
            subject: user.id,
            issuer: 'API TechU',
            audience: 'API TechU',
            jwtid: uuidv4(),
            expiresIn: '1h'
          };
          const token = jwt.sign(payload, process.env.JWT_SECRETKEY, options);

          msg = {'token': token, '2fa': user['2fa']};
        } else {
          status = 401;
          msg = {msg: 'Authentication error'};
        }

        const exp = Math.floor(Date.now() / 1000)
          ;
        console.log(exp);
        //on login remove blacklisted expired tokens
        User.findByIdAndUpdate(user.id, {$pull: {invalidTokens: {$elemMatch: {exp: {$lte: exp}}}}},
          {safe: true, new: true},
          function(err, user) {console.log(user)});
        res.status(status).send(msg);
      });
    } else {
      status = 404;
      res.status(status).send({msg: 'User not found'});
    }
  });
}

function logoutV1(req, res) {
  console.log('POST /apitechu/v1/logout');

  if (req.decodedToken) {
    User.findByIdAndUpdate(req.decodedToken.sub,
      {$push: {invalidTokens: {jti: req.decodedToken.jti, exp: req.decodedToken.exp}}},
      {safe: true, new: true},
      function (err, user) {
        console.log(err);
        console.log(user);
        if (!err && user) {
          res.status(200).send({msg: 'User logged out'});
        } else {
          res.status(404).send({msg: 'User not found'});
        }
      });
  } else {
    res.status(401).send({msg: 'Unauthorized access'});
  }
}

function validateTokenV1(req, res, next) {
  console.log('validateTokenV1');

  const authHeader = req.headers['authorization'];

  console.log('Authorization: ' + authHeader);
  if (authHeader) {
    bearerToken = authHeader.split(" ");

    console.log(bearerToken.length);
    if (bearerToken.length == 2) {
      jwt.verify(bearerToken[1], process.env.JWT_SECRETKEY, function(err, decodedToken) {
        if (err) {
          return res.status(401).send({msg: 'Invalid token', error: err });
        }

        console.log("decodedToken: ", decodedToken);
        User.findOne({_id: decodedToken.sub, 'invalidTokens.jti': decodedToken.jti}, (err, user) => {
          if (err || user) {
            return res.status(401).send({msg: 'Invalid token', error: err });
          }

          req.decodedToken = decodedToken;
          next();
        });
      });
    }
  } else {
    res.status(401).send({msg: 'Missing authorization header'});
  }
}

module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
module.exports.validateTokenV1 = validateTokenV1;

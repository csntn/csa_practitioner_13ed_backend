require('dotenv').config();
const https = require('https');
const express = require('express');
const app = express();

const enableCORS = function(req, res, next) {
    res.set("Access-Control-Allow-Origin", "*");
    res.set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
    res.set("Access-Control-Allow-Headers", "Content-Type, Authorization");
    next();

};

const db = require('./models/db');
const userController = require('./controllers/usercontroller');
const accountController = require('./controllers/accountcontroller');
const authController = require('./controllers/authcontroller');
const movementController = require('./controllers/movementcontroller');

app.use(express.json());
app.use(enableCORS);

const port = process.env.PORT || 3000;
app.listen(port);
const httpsOptions = {
  key: process.env.HTTPS_KEY,
  cert: process.env.HTTPS_CERT
}
https.createServer(httpsOptions, app).listen(port+1);
console.log('API escuchando en el puerto ' + port);

//dev unprotected endpoints
app.get('/apitechu/v1/users', userController.getUsersV1);
app.get('/apitechu/v1/accounts', accountController.getAccountsV1);
app.delete('/apitechu/v1/accounts/:accountid', accountController.deleteAccountByIdV1);
app.delete('/apitechu/v1/users/:userid', userController.deleteUserByIdV1);
app.get('/apitechu/v1/users/:userid', userController.getUserByIdV1);
app.post('/apitechu/v1/users/:userid/accounts', accountController.createUserByIdAccountV1);
app.delete('/apitechu/v1/users/:userid/accounts', accountController.deleteUserByIdAccountsV1);
app.delete('/apitechu/v1/users/:userid/accounts/:accountid', accountController.deleteUserByIdAccountByIdV1);
app.get('/apitechu/v1/users/:userid/accounts', accountController.getUserByIdAccountsV1);
app.get('/apitechu/v1/users/:userid/accounts/:accountid', accountController.getUserByIdAccountByIdV1);

app.get('/apitechu/v1/movements', movementController.getMovementsV1);
app.post('/apitechu/v1/movements', movementController.createMovementV1);
app.delete('/apitechu/v1/movements/:movementid', movementController.deleteMovementByIdV1);

//login and register
app.post('/apitechu/v1/login', authController.loginV1);
app.post('/apitechu/v1/users', userController.createUserV1);

//protected endpoints
app.post('/apitechu/v1/logout', authController.validateTokenV1, authController.logoutV1);
app.get('/apitechu/v1/user', authController.validateTokenV1, userController.getUserV1);
app.delete('/apitechu/v1/user', authController.validateTokenV1, userController.deleteUserV1);
app.post('/apitechu/v1/user/accounts', authController.validateTokenV1, accountController.createUserAccountV1);
app.delete('/apitechu/v1/user/accounts', authController.validateTokenV1, accountController.deleteUserAccountsV1);
app.delete('/apitechu/v1/user/accounts/:accountid', authController.validateTokenV1, accountController.deleteUserAccountByIdV1);
app.get('/apitechu/v1/user/accounts', authController.validateTokenV1, accountController.getUserAccountsV1);
app.get('/apitechu/v1/user/accounts/:accountid', authController.validateTokenV1, accountController.getUserAccountByIdV1);
app.get('/apitechu/v1/user/accounts/:accountid/movements', authController.validateTokenV1, movementController.getUserAccountByIdMovementsV1);
app.post('/apitechu/v1/user/accounts/:accountid/movements', authController.validateTokenV1, movementController.createUserAccountByIdMovementV1);
